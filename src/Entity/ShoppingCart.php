<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;




/**
 * @ORM\Entity(repositoryClass="App\Repository\ShoppingCartRepository")
 */

class ShoppingCart
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="purchase", type="boolean", nullable=true)
     */
    private $purchase;

    /**
     * @var int
     *
     * @ORM\Column(name="total", type="integer", nullable=true)
     */
    private $total;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="shoppingCart", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductLine", mappedBy="shoppingCart", orphanRemoval=true, cascade={"persist"})
     */
    private $productLine;


    public function __construct()
    {
        $this->productLine = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPurchase(): ?bool
    {
        return $this->purchase;
    }

    public function setPurchase(bool $purchase): self
    {
        $this->purchase = $purchase;

        return $this;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|ProductLine[]
     */
    public function getProductLine(): Collection
    {
        return $this->productLine;
    }

    public function addProductLine(ProductLine $productLine): self
    {
        if (!$this->productLine->contains($productLine)) {
            $this->productLine[] = $productLine;
            $productLine->setShoppingCart($this);
        }

        return $this;
    }

    public function removeProductLine(ProductLine $productLine): self
    {
        if ($this->productLine->contains($productLine)) {
            $this->productLine->removeElement($productLine);
            // set the owning side to null (unless already changed)
            if ($productLine->getShoppingCart() === $this) {
                $productLine->setShoppingCart(null);
            }
        }

        return $this;
    }

 
}
