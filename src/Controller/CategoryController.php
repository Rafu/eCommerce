<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ProductType;
use App\Form\CategoryType;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Category;
use App\Repository\CategoryRepository;

class CategoryController extends Controller
{    //Enregistre les catégories
    /**
     * @Route("admin/category", name="category")
     */
    public function index(Request $req)
    {

        $form = $this->createForm(CategoryType::class);
        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {


            $em = $this->getDoctrine()->getManager();
            $em->persist($form->getData());
            $em->flush();

            return new Response('catégorie ajoutée');

        }
        return $this->render('category/index.html.twig', [
            'form' => $form->createView(),

        ]);

    }
    //affiche les catégories et sous-catégories générées dans le header et le footer

    /**
     * @Route("admin/showCategory/{category}", name = "showFoot")
     */
    public function showCat(bool $header = false, Category $category = null)
    {
        $repo = $this->getDoctrine()->getRepository(Category::class);
        $categories = $repo->findBy(["category" => null]);

        if ($header) {
            return $this->render('_header.html.twig', [
                'categories' => $categories,
                'category' => $category
            ]);

        } else {
            return $this->render('_footer.html.twig', [
                'categories' => $categories,
                'category' => $category
            ]);

        }
    }

}

