<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\ProductType;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Product;
use App\Entity\Category;
use App\Entity\CategoryType;
use Symfony\Component\HttpFoundation\File\File;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManager;
// use Symfony\Component\HttpFoundation\File\UploadedFile;


class ProductController extends Controller
{
    //ajout de produits dans la base de données//
    /**
     * @Route("/admin/addProduct", name="AddProduct")
     */
    public function index(Request $req)
        {
        $product = new Product;
        $product->getCategory(new Category());
        $form = $this->createForm(ProductType::class , $product);
        $form->handleRequest($req);

        if($form->isSubmitted()&& $form->isValid()){

         $fileName = $this->generateUniqueFileName().'.'.$product->getImage()->guessExtension();
         $product->getImage()->move(dirname(__FILE__)."/../../public/uploads", $fileName);
         $product->setImage("uploads/".$fileName);

     
          $em = $this->getDoctrine()->getManager();
          $em->persist($form->getData());
          $em->flush();

          return $this->redirectToRoute("AddProduct");

        }
        return $this->render('product/index.html.twig', [
           'form' => $form->createView(),
           'editMode'=>$product->getId()!== null
        ]);
       
    }

    private function generateUniqueFileName()
    {
        return md5(uniqid());
    }

    //modification de produits//
    /**
     * @Route("/admin/update/{id}", name="updProd")
     */
    public function update(string $id, ProductRepository $repo, Request $request){


        $product = $repo->find($id);
        $product->setImage(new File($product->getimage()));
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);


        if($form->isSubmitted()&& $form->isValid()){

         
        $fileName = $this->generateUniqueFileName().'.'.$product->getImage()->guessExtension();
        $product->getImage()->move(dirname(__FILE__)."/../../public/uploads", $fileName);
        $product->setImage("uploads/".$fileName);
      
             
          $em = $this->getDoctrine()->getManager();
          $em->persist($form->getData());
          $em->flush();

          return $this->redirectToRoute("admin");

        }
        dump($product);
            return $this->render('product/index.html.twig', [
            "form" => $form->createView(),
             "prod" => $product,
             "editMode"=>$product->getId()!== null
            ]);
         }
    
         
     //Suppression de produits//
     /**
     * @Route("/admin/rmvProduct/{id}", name="removeProduct")
     */
    public function remove(int $id){

        $em = $this->getDoctrine()->getEntityManager();
        $prod = $em->getRepository(Product::class)->find($id); 
        $em->remove($prod);
        $em->flush();
        return $this->redirectToRoute("admin");
    }
    
}
