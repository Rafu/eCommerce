<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TermsController extends Controller
{
    // affichage des conditions générales de vente
    /**
     * @Route("/terms", name="terms")
     */
    public function index()
    {
        return $this->render('terms/index.html.twig', [
            'controller_name' => 'TermsController',
        ]);
    }
}
