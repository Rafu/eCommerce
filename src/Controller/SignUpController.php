<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Form\UserType;
use App\Entity\Address;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\AddressRepository;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;


class SignUpController extends Controller
{
    //Création et modification de compte client//
    /**
     * @Route("/sign/up", name="sign_up")
     * @Route("/accountUpd/{id}", name="compte")
     */
    public function index(user $user = null, Request $request, UserPasswordEncoderInterface $encoder, UserRepository $repo)
    {
        if (!$user) {
            $user = new User();
            $user->addAddress(new Address());
        }

        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            dump($user);
            return $this->redirectToRoute("home");
        }

        return $this->render('sign_up/index.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
            // 'address'=>$address
        ]);
    }
  
     //Affichage du compte client depuis l'icône user du header //
    /**
     * @Route("/user", name="user")
     */
    public function account()
    {

        $repo = $this->getDoctrine()->getRepository(User::class);

        return $this->render('compte/compte.html.twig', [
        ]);
    }

}

