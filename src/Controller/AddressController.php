<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\AddressType;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Address;

class AddressController extends Controller
{
    /**
     * @Route("/address", name="address")
     */
    public function index(Request $req){
   
        $address = new Address;
        $form = $this->createForm(AddressType::class , $address);
        $form->handleRequest($req);

        if($form->isSubmitted()&& $form->isValid()){

     
        $em = $this->getDoctrine()->getManager();
          $em->persist($form->getData());
           $em->flush();

        }
        return $this->render('address/index.html.twig', [
           'form' => $form->createView()
        ]);
    }
}
