<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ProductRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\ShoppingCart;
use App\Entity\ProductLine;
use App\Entity\Product;
use App\Repository\ProductLineRepository;


class ProductLineController extends Controller
{
    //Ajout de produits dans la "ligne panier"//
    /**
     * @Route("/user/{id}/{number}/product-line", name="product_line")
     */

    public function index(ProductRepository $repo, int $number, ObjectManager $manager, Session $session, int $id, UserInterface $user, ProductLineRepository $productLine, Product $product )
    {

        $shoppingCart = $session->get("cart");
        if (!$shoppingCart) {
            dump("no");
            $shoppingCart = new ShoppingCart();
        } else {
            dump("yes");
            $shoppingCart = $manager->merge($shoppingCart);
        }
 
        $found = false;
        foreach ($shoppingCart->getProductLine() as $pLine) {

            if ($pLine->getProduct()->getId() === $product->getId()) {

                $pLine->setQuantity($pLine->getQuantity() + $number);
                $price = $product->getPrice() * $pLine->getQuantity();
                $pLine->setPrice($price);

                $found = true;
                
            }
        
        }
        if (!$found) {


            $productLine = new ProductLine();
            $prod = $repo->find($id);
            $productLine->setProduct($prod);
            $productLine->setQuantity($number);
            $price = $prod->getPrice() * $number;
            $productLine->setPrice($price);
            $shoppingCart->addProductLine($productLine);
            $shoppingCart->setUser($user);

        }

        $manager->persist($shoppingCart);
        $manager->flush();
        $session->set("cart", $shoppingCart);

        return $this->redirectToRoute("shopping_cart", []);

        return $this->render('base.html.twig', []);

    }

    //Suppression d'une ligne panier//
     /**
     * @Route("/user/{id}/rmv-productLine", name = "removeProduct-line")
     */

    public function removeProductLine(Productline $productline){

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($productline);
        $em->flush();
        

        return $this->redirectToRoute("shopping_cart", []);
    }

    }

     
       

