<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use App\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,["label"=>"prenom"])
            ->add('surname',TextType::class,["label"=>"nom"])
            ->add('email',TextType::class,["label"=>"email"])
            ->add('phonenumber',TextType::class,["label"=>"telephone"])
            ->add('password', RepeatedType::class, [
                "type"=> PasswordType::class,
                "first_options"=> ["label" => "Mot de passe"],
                "second_options"=> ["label" => "Confirmer le Mot de passe"],
                "invalid_message"=> "Password doesn't match"

            ])
            ->add("address", CollectionType::class, [
                "entry_type"=> AddressType::class, 'allow_add'=> true,
                'entry_options' => array('label' => false),
                "label"=>false
            ])
            ->add("termsAccepted", CheckboxType::class, [
                'label'=> 'accepter les CGU',
                'mapped' => false,
                'constraints' => new IsTrue(),]
            )
        ;
    }
 
 
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
