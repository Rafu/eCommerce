-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: db
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.34-MariaDB-1~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D34A04AD9777D11E` (`category_id`),
  CONSTRAINT `FK_D34A04AD12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,13,'Sac à dos.','Skate qui vole, magique, un peu comme un tapis volant, mais sans les franges',8,'uploads/c0b4632e523abc1140fbac8fdb9e46b4.png'),(2,13,'Sac à dos.','Sac à dos en coton, trois compartiments et poche zippé sur le devant. Brides règlables.',66,'uploads/3b6ec75d65cadc58f60b0f6f7579666b.png'),(3,12,'Baskets','Baskets en coton, semelle polymère.',73,'uploads/5bc9ca3850a77988b1bc47a77a07d4bd.png'),(4,10,'Sweat-shirt','Sweat-shirt bien joli et tout, avec des man,ches, tout moelleux, tiens chaud en hiver comme en été, du cou, il est déconseillé de le porter en été => rapport à la chaleur.',54,'uploads/cf01785c9e03d7a72df2995050951935.png'),(5,18,'Skate Rose','Skate rose mais unisexe quand même, 2018 quand même!',123,'uploads/0dc140666a47913d9d98edbefcd57270.jpeg'),(6,3,'Tee-shirt très chouette','Tee-shirt col rond, en tissus, bien chouette. 100% coton. Manches courtes, parcequ\'il fait chaud.',45,'uploads/0418995409eafeaba9d1e39a051663b7.png'),(7,8,'Baskets','Bleues avec des petits carreaux genre damier mais sans les pions.',87,'uploads/b6dfa6ff48fad3b7583f49b39ddfb5a6.png'),(8,14,'Casquette','Skate qui vole, magique, un peu comme un tapis volant, mais sans les franges',57,'uploads/1bb86702d40a7d87686f111283ff0bf3.png'),(9,15,'Bonnet','Sweat-shirt bien joli et tout, avec des man,ches, tout moelleux, tiens chaud en hiver comme en été, du cou, il est déconseillé de le porter en été => rapport à la chaleur.',43,'uploads/21c5b1df925f69c7812f14c9a8369666.png'),(10,6,'Sweat-shirt','Skate qui vole, magique, un peu comme un tapis volant, mais sans les franges',79,'uploads/395a832e96ddfbe954364c08cddd9c9a.jpeg'),(11,9,'Tee-shirt très chouette','Tee-shirt fait en tissus, bien chouette. 100% coton. Manches courtes, blablabla, et aussi bla bla bli.  J\'aime bien les tee-shirts.',76,'uploads/44f6fc519846c4fa5750c11fc5ad8117.jpeg'),(12,18,'Skate Bleu','Skate qui vole, magique, un peu comme un tapis volant, mais sans les franges',79,'uploads/54ade23170ef82e8f112301095626feb.jpeg'),(13,3,'Tee-shirt très chouette','Tee-shirt fait en tissus, bien chouette. 100% coton. Manches courtes, blablabla, et aussi bla bla bli.  J\'aime bien les tee-shirts.',67,'uploads/963e362650b189a8ddd6b3bb026c329d.jpeg');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-07 17:38:10
